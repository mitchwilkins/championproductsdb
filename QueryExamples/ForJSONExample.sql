﻿SELECT TOP 10
       [object_id] as Id
      ,[name]
	  ,[Create_date]
  FROM sys.tables 
   FOR JSON PATH


;WITH cteJson (database_id,[name]) AS
   (  
     SELECT database_id
	     ,[name]
       FROM sys.databases 
    )  
SELECT database_id
      ,[name]
  FROM cteJson
   FOR JSON PATH