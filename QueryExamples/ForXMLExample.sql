﻿select TOP 10
       [object_id] as Id
      ,[name]
	  ,[Create_date]
  from sys.tables 
   FOR XML PATH('Table')

;WITH cteXml (database_id,[name]) AS
   (  
     SELECT database_id
	     ,[name]
       FROM sys.databases 
    )  
SELECT database_id
      ,[name]
  FROM cteXml
   FOR XML PATH