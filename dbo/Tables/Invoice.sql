﻿CREATE TABLE [dbo].[Invoice] (
    [InvoiceId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [CustomerId]    INT             NOT NULL,
    [InvoiceAmount] DECIMAL (18, 2) CONSTRAINT [DF_Invoice_InvoiceAmount] DEFAULT ((0.00)) NOT NULL,
    [PaidDate]      DATETIME        NULL,
    [CreatedDate]   DATETIME        CONSTRAINT [DF_Invoice_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]   DATETIME        NULL,
    CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED ([InvoiceId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Invoice_CustomerId]
    ON [dbo].[Invoice]([CustomerId] ASC);

